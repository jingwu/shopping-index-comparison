# Estimating Dietary Intake from Grocery Purchase Data - A Comparative Validation of Relevant Indicators.

This repository is about the supplementary materials of the paper: Estimating Dietary Intake from Grocery Purchase Data - A Comparative Validation of Relevant Indicators. 

We have uploaded the data and the code. The data was retrieved on 2021.06.19. If you have any further questions, please contact Jing Wu, jing.wu@unisg.ch.
